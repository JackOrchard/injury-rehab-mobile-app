package com.example.rehabhelper.viewmodels

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider


class InjuryViewModelFactory(private val application: Application, private val injuryId : String) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return InjuryViewModel(application, injuryId) as T
    }

}