package com.example.rehabhelper.viewmodels

import android.app.Application
import androidx.lifecycle.*
import com.example.rehabhelper.database.InjuryDatabase
import com.example.rehabhelper.database.InjuryRepository
import com.example.rehabhelper.models.Injury
import com.google.android.gms.location.ActivityTransition
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class InjuriesViewModel(application: Application) : AndroidViewModel(application) {

    private val repository : InjuryRepository

    val allInjuries : LiveData<List<Injury>>


    init {
        val injuryDao =  InjuryDatabase.getDatabase(application).injuryDao()
        repository = InjuryRepository(injuryDao)
        allInjuries = repository.allInjuries
    }

    fun insert(injury: Injury) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(injury)
    }

    fun delete(injury: Injury) = viewModelScope.launch(Dispatchers.IO) {
        repository.delete(injury)
    }
}