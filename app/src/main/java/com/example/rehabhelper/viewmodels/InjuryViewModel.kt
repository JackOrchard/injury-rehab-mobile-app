package com.example.rehabhelper.viewmodels

import android.app.Application
import android.net.Uri
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import androidx.preference.PreferenceManager
import com.example.rehabhelper.R
import com.example.rehabhelper.database.InjuryDatabase
import com.example.rehabhelper.database.InjuryRepository
import com.example.rehabhelper.models.Injury
import com.google.android.gms.location.ActivityTransition
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File

class InjuryViewModel(application: Application, injuryId : String) : AndroidViewModel(application) {

    private val repository : InjuryRepository
    private val applicationInstance = application

    val injury : LiveData<Injury>
    val transitions = mutableListOf<ActivityTransition>()

    init {
        val injuryDao =  InjuryDatabase.getDatabase(application).injuryDao()
        repository = InjuryRepository(injuryDao)
        injury = repository.getInjury(injuryId)
    }

    fun update() = viewModelScope.launch(Dispatchers.IO) {
        if (injury.value != null) {
            repository.update(injury.value!!)
        }
    }

    /*fun updateDate(injuryUpdated : String) {
        println(injuryUpdated)
        println(injury.value!!.date)
        if (injury.value == null || injuryUpdated == injury.value!!.date) {
            return
        } else {
            update()
        }
    }*/

    /*fun updateName(injuryUpdated : String) {
        println(injuryUpdated)
        println(injury.value!!.name)
        if (injury.value == null || injuryUpdated == injury.value!!.name) {
            return
        } else {
            update()
        }
    }*/

    fun getAndUpdateInjury(injuryId: String, fileName: String) = viewModelScope.launch(Dispatchers.IO) {
        val injuryStatic = repository.getStaticInjury(injuryId)
        injuryStatic.imageUrl = fileName
        repository.update(injuryStatic)
    }

    fun getEmailTitle() : String {
        var emailTitle = applicationInstance.getString(R.string.update) + ": "
        emailTitle += injury.value?.name

        return emailTitle
    }

    fun getEmailBody(isWalking : Boolean, isRunning  : Boolean, isBiking : Boolean): String {
        var emailBody = applicationInstance.getString(R.string.email_header) + "\n\n"
        emailBody += applicationInstance.getString(R.string.email_body_start) + "\n\n"
        emailBody += applicationInstance.getString(R.string.email_body_activity) + "\n"
        if (isWalking) {
            emailBody += "- " + applicationInstance.getString(R.string.walking) + "\n"
        }
        if (isBiking) {
            emailBody += "- " + applicationInstance.getString(R.string.biking) + "\n"
        }
        if (isRunning) {
            emailBody += "- " + applicationInstance.getString(R.string.running) + "\n"
        }
        emailBody += "\n"
        emailBody += applicationInstance.getString(R.string.email_body_rehab) + ": " +  injury.value!!.rehab!!.timesCompleted + "\n"


        emailBody += "\n"

        emailBody += applicationInstance.getString(R.string.email_end)

        return emailBody
    }



}