package com.example.rehabhelper.views

import android.os.AsyncTask
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.*
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.rehabhelper.R
import com.example.rehabhelper.adapters.EXTRA_INJURY
import com.example.rehabhelper.adapters.PageCollectionAdapter
import com.example.rehabhelper.viewmodels.InjuryViewModel
import com.example.rehabhelper.viewmodels.InjuryViewModelFactory


class RehabFragment() : Fragment() {
    private lateinit var editText : EditText
    private lateinit var editText2 : EditText
    private lateinit var editText3 : EditText
    private lateinit var editText4 : EditText
    private lateinit var editText5 : EditText

    private lateinit var progressBar: ProgressBar
    private lateinit var progressBar2 : ProgressBar
    private lateinit var progressBar3 : ProgressBar
    private lateinit var progressBar4 : ProgressBar
    private lateinit var progressBar5 : ProgressBar

    private lateinit var star: ImageView
    private lateinit var star2 : ImageView
    private lateinit var star3 : ImageView
    private lateinit var star4 : ImageView
    private lateinit var star5 : ImageView

    private lateinit var injuryViewModel: InjuryViewModel

    private val timerDuration : Long = 30000
    private val timerInterval : Long = 500

    var cTimer: CountDownTimer? = null
    var exercisesCompleted = 0





    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.rehab_fragment, container, false)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        cancelTimer()
        exercisesCompleted = 0
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        editText = view.findViewById(R.id.editText)
        editText2 = view.findViewById(R.id.editText2)
        editText3 = view.findViewById(R.id.editText3)
        editText4 = view.findViewById(R.id.editText4)
        editText5 = view.findViewById(R.id.editText5)

        progressBar = view.findViewById(R.id.progressBar)
        progressBar2 = view.findViewById(R.id.progressBar2)
        progressBar3 = view.findViewById(R.id.progressBar3)
        progressBar4 = view.findViewById(R.id.progressBar4)
        progressBar5 = view.findViewById(R.id.progressBar5)

        star = view.findViewById(R.id.star)
        star2 = view.findViewById(R.id.star2)
        star3 = view.findViewById(R.id.star3)
        star4 = view.findViewById(R.id.star4)
        star5 = view.findViewById(R.id.star5)


        val injuryId = arguments!!.getString(EXTRA_INJURY)
        injuryViewModel = InjuryViewModelFactory(activity!!.application, injuryId!!).create(InjuryViewModel::class.java)


        injuryViewModel.injury.observe(viewLifecycleOwner, Observer {injury ->
            editText.setText( injury.rehab!!.exerciseOne)
            editText2.setText( injury.rehab!!.exerciseTwo)
            editText3.setText( injury.rehab!!.exerciseThree)
            editText4.setText( injury.rehab!!.exerciseFour)
            editText5.setText( injury.rehab!!.exerciseFive)
        })









        progressBar.setOnClickListener {
            cancelTimer()
            startTimer(progressBar, star)
        }

        progressBar2.setOnClickListener {
            cancelTimer()
            startTimer(progressBar2, star2)
        }

        progressBar3.setOnClickListener {
            cancelTimer()
            startTimer(progressBar3, star3)
        }

        progressBar4.setOnClickListener {
            cancelTimer()
            startTimer(progressBar4, star4)
        }

        progressBar5.setOnClickListener {
            cancelTimer()
            startTimer(progressBar5, star5)
        }

        editText.setOnEditorActionListener { v, actionId, event ->
            if (actionId === EditorInfo.IME_ACTION_DONE) {
                if (injuryViewModel.injury.value != null && injuryViewModel.injury.value!!.rehab!!.exerciseOne != editText.text.toString()) {
                    injuryViewModel.injury.value!!.rehab!!.exerciseOne = editText.text.toString()
                    injuryViewModel.update()
                }
            }
            false
        }

        editText.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                if (injuryViewModel.injury.value != null && injuryViewModel.injury.value!!.rehab!!.exerciseOne != editText.text.toString()) {
                    injuryViewModel.injury.value!!.rehab!!.exerciseOne = editText.text.toString()
                    injuryViewModel.update()
                }
            }
        }

        editText2.setOnEditorActionListener { v, actionId, event ->
            if (actionId === EditorInfo.IME_ACTION_DONE) {
                if (injuryViewModel.injury.value != null && injuryViewModel.injury.value!!.rehab!!.exerciseTwo != editText2.text.toString()) {
                    injuryViewModel.injury.value!!.rehab!!.exerciseTwo = editText2.text.toString()
                    injuryViewModel.update()
                }
            }
            false
        }

        editText2.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                if (injuryViewModel.injury.value != null && injuryViewModel.injury.value!!.rehab!!.exerciseTwo != editText2.text.toString()) {
                    injuryViewModel.injury.value!!.rehab!!.exerciseTwo = editText2.text.toString()
                    injuryViewModel.update()
                }
            }
        }

        editText3.setOnEditorActionListener { v, actionId, event ->
            if (actionId === EditorInfo.IME_ACTION_DONE) {
                if (injuryViewModel.injury.value != null && injuryViewModel.injury.value!!.rehab!!.exerciseThree != editText3.text.toString()) {
                    injuryViewModel.injury.value!!.rehab!!.exerciseThree = editText3.text.toString()
                    injuryViewModel.update()
                }
            }
            false
        }

        editText3.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                if (injuryViewModel.injury.value != null && injuryViewModel.injury.value!!.rehab!!.exerciseThree != editText3.text.toString()) {
                    injuryViewModel.injury.value!!.rehab!!.exerciseThree = editText3.text.toString()
                    injuryViewModel.update()
                }
            }
        }

        editText4.setOnEditorActionListener { v, actionId, event ->
            if (actionId === EditorInfo.IME_ACTION_DONE) {
                if (injuryViewModel.injury.value != null && injuryViewModel.injury.value!!.rehab!!.exerciseFour != editText4.text.toString()) {
                    injuryViewModel.injury.value!!.rehab!!.exerciseFour = editText4.text.toString()
                    injuryViewModel.update()
                }
            }
            false
        }

        editText4.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                if (injuryViewModel.injury.value != null && injuryViewModel.injury.value!!.rehab!!.exerciseFour != editText4.text.toString()) {
                    injuryViewModel.injury.value!!.rehab!!.exerciseFour = editText4.text.toString()
                    injuryViewModel.update()
                }
            }
        }

        editText5.setOnEditorActionListener { v, actionId, event ->
            if (actionId === EditorInfo.IME_ACTION_DONE) {
                if (injuryViewModel.injury.value != null && injuryViewModel.injury.value!!.rehab!!.exerciseFive != editText5.text.toString()) {
                    injuryViewModel.injury.value!!.rehab!!.exerciseFive = editText5.text.toString()
                    injuryViewModel.update()
                }
            }
            false
        }

        editText5.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                if (injuryViewModel.injury.value != null && injuryViewModel.injury.value!!.rehab!!.exerciseFive != editText5.text.toString()) {
                    injuryViewModel.injury.value!!.rehab!!.exerciseFive = editText5.text.toString()
                    injuryViewModel.update()
                }
            }
        }




    }


    //start timer function
    fun startTimer(currentProgressBar: ProgressBar, imageView: ImageView) {
        cTimer = object : CountDownTimer(timerDuration, timerInterval) {
            override fun onTick(millisUntilFinished: Long) {
                val millisDone = timerDuration - millisUntilFinished
                val progress : Float = (millisDone.toFloat()/timerDuration.toFloat()) * 100
                currentProgressBar.progress = progress.toInt()
            }
            override fun onFinish() {
                currentProgressBar.progress = 0
                val toast = Toast.makeText(context!!, "Exercise done!", Toast.LENGTH_SHORT)
                toast.show()
                imageView.isVisible = true
                exercisesCompleted += 1
                imageView.setImageResource(R.drawable.ic_star_black_24dp)
                if (exercisesCompleted == 5) {
                    injuryViewModel.injury.value!!.rehab!!.timesCompleted += 1
                    injuryViewModel.update()
                    exercisesCompleted = 0
                    val toast = Toast.makeText(context!!, "Set Completed!", Toast.LENGTH_LONG)
                    toast.show()
                    resetStars()
                }
            }
        }.start()
    }


    //cancel timer
    fun cancelTimer() {
        if (cTimer != null) cTimer?.cancel()
    }

    fun resetStars() {
        star.setImageResource(R.drawable.ic_star_border_black_24dp)
        star2.setImageResource(R.drawable.ic_star_border_black_24dp)
        star3.setImageResource(R.drawable.ic_star_border_black_24dp)
        star4.setImageResource(R.drawable.ic_star_border_black_24dp)
        star5.setImageResource(R.drawable.ic_star_border_black_24dp)

    }

}