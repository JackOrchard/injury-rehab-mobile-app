package com.example.rehabhelper.views

import android.Manifest
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.preference.PreferenceManager
import com.example.rehabhelper.R
import com.example.rehabhelper.adapters.EXTRA_INJURY
import com.example.rehabhelper.viewmodels.InjuryViewModel
import com.example.rehabhelper.viewmodels.InjuryViewModelFactory
import java.io.File


private const val TRANSITIONS_RECEIVER_ACTION = "TRANSITIONS_RECEIVER_ACTION"

class InjuryFragment() : Fragment() {
    private lateinit var injuryText : EditText
    private lateinit var injuryDate : EditText
    private lateinit var injuryPicture : ImageView
    private lateinit var emailButton : Button

    private lateinit var injuryViewModel: InjuryViewModel
    val REQUEST_CODE_PERMISSIONS = 11
    private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.ACTIVITY_RECOGNITION)


    private val photoDirectory
    //get() = File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "backlog")
    get() = File(Environment.getExternalStorageDirectory(), "rehab")


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.injury_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }




    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        injuryText = view.findViewById(R.id.injuryName)
        injuryDate = view.findViewById(R.id.injuryDate)
        injuryPicture = view.findViewById(R.id.injuryImage)
        emailButton = view.findViewById(R.id.sendEmail)

        val injuryId = arguments!!.getString(EXTRA_INJURY)
        println(injuryId)

        injuryViewModel = InjuryViewModelFactory(activity!!.application, injuryId!!).create(InjuryViewModel::class.java)


        injuryViewModel.injury.observe(viewLifecycleOwner, Observer { injury ->
            injuryText.setText(injury.name)
            injuryDate.setText(injury.date)
            val injuryId = injury.id
            val file = File(activity?.externalMediaDirs?.first(),
                "injury_${injuryId}.jpg")
            if (file.exists()) {
                val bitmap = BitmapFactory.decodeFile(file.absolutePath)
                injuryPicture.setImageBitmap(bitmap)
            } else {
                injuryPicture.setImageResource(android.R.drawable.ic_menu_camera)
            }





            injuryPicture.setOnClickListener {
                val intent = Intent(context, CameraActivity::class.java)
                intent.putExtra(EXTRA_INJURY, injury.id.toString())
                context!!.startActivity(intent)
            }

            emailButton.setOnClickListener {

                val preferences = PreferenceManager.getDefaultSharedPreferences(context)
                val isWalking = preferences.getBoolean("walking_switch", false)
                val isBiking = preferences.getBoolean("biking_switch", false)
                val isRunning = preferences.getBoolean("running_switch", false)

                val file = File(activity?.externalMediaDirs?.first(),
                    injury.imageUrl!!)

                var intent = Intent(Intent.ACTION_SENDTO) // it's not ACTION_SEND
                intent.putExtra(Intent.EXTRA_SUBJECT, injuryViewModel.getEmailTitle())
                intent.putExtra(Intent.EXTRA_TEXT, injuryViewModel.getEmailBody(isWalking, isRunning, isBiking))


                if(file.exists() && injury.imageUrl != "") {
                    val imageUri = Uri.fromFile(file)
                    println("image_exists")
                    intent.putExtra(Intent.EXTRA_STREAM, imageUri);
                }

                intent.data = Uri.parse("mailto:") // or just "mailto:" for blank
                //intent.setType("text/plain");
                //intent.setType("image/png");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK) // this will make such that when user returns to your app, your app is displayed, instead of the email app.

                context?.startActivity(intent)
            }


        })


        injuryText.setOnEditorActionListener { v, actionId, event ->
            if (actionId === EditorInfo.IME_ACTION_DONE) {
                if (injuryViewModel.injury.value != null && injuryViewModel.injury.value!!.name != injuryText.text.toString()) {
                    injuryViewModel.injury.value!!.name = injuryText.text.toString()
                    injuryViewModel.update()
                }
            }
            false
        }

        injuryText.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                if (injuryViewModel.injury.value != null && injuryViewModel.injury.value!!.name != injuryText.text.toString()) {
                    injuryViewModel.injury.value!!.name = injuryText.text.toString()
                    injuryViewModel.update()
                }
            }
        }

        injuryDate.setOnEditorActionListener { v, actionId, event ->
            if (actionId === EditorInfo.IME_ACTION_DONE) {
                if (injuryViewModel.injury.value != null && injuryViewModel.injury.value!!.date != injuryDate.text.toString()) {
                    injuryViewModel.injury.value!!.date = injuryDate.text.toString()
                    injuryViewModel.update()
                }
            }
            false
        }

        injuryDate.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                if (injuryViewModel.injury.value != null && injuryViewModel.injury.value!!.date != injuryDate.text.toString()) {
                    injuryViewModel.injury.value!!.date = injuryDate.text.toString()
                    injuryViewModel.update()
                }
            }
        }


    }








}
