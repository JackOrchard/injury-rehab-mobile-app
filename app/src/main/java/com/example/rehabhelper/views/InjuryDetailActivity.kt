package com.example.rehabhelper.views

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.example.rehabhelper.R
import com.example.rehabhelper.adapters.EXTRA_INJURY
import com.example.rehabhelper.adapters.PageCollectionAdapter
import com.example.rehabhelper.viewmodels.InjuriesViewModel
import com.example.rehabhelper.viewmodels.InjuryViewModel
import com.example.rehabhelper.viewmodels.InjuryViewModelFactory
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class InjuryDetailActivity : AppCompatActivity() {

    private lateinit var demoCollectionAdapter: PageCollectionAdapter
    private lateinit var viewPager: ViewPager2
    private lateinit var injuryViewModel: InjuryViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_injury)


        setSupportActionBar(findViewById(R.id.toolbar2))

        val injuryId = intent.getStringExtra(EXTRA_INJURY)


        demoCollectionAdapter = PageCollectionAdapter(supportFragmentManager, lifecycle, injuryId)


    }

    override fun onStart() {
        super.onStart()
        viewPager = findViewById(R.id.pager)
        viewPager.adapter = demoCollectionAdapter

        val tabLayout = findViewById<TabLayout>(R.id.tab_layout)
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            when (position) {
                0 -> tab.text = "Injury"
                1 -> tab.text = "Rehab"
            }
        }.attach()
    }

    fun updateInjury() {
        injuryViewModel.update()
    }
}