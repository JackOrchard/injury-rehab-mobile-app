package com.example.rehabhelper.views

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.rehabhelper.R
import com.example.rehabhelper.SettingsFragment

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)
        setSupportActionBar(findViewById(R.id.settings_toolbar))
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.preferences_container, SettingsFragment())
            .commit()
    }




}