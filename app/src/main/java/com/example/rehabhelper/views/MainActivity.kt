package com.example.rehabhelper.views

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.rehabhelper.R
import com.example.rehabhelper.adapters.InjurysListAdapter
import com.example.rehabhelper.adapters.OnStartDragListener
import com.example.rehabhelper.adapters.SimpleItemTouchHelperCallback
import com.example.rehabhelper.models.Injury
import com.example.rehabhelper.models.Rehab
import com.example.rehabhelper.viewmodels.InjuriesViewModel
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


class MainActivity : AppCompatActivity(), OnStartDragListener {

    private lateinit var injuryList : RecyclerView
    private lateinit var injurysListAdapter: InjurysListAdapter
    private lateinit var injuriesViewModel: InjuriesViewModel
    private lateinit var mItemTouchHelper : ItemTouchHelper


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        injuryList = findViewById(R.id.injuryRecycler)
        val layoutManager = LinearLayoutManager(this)
        injuryList.layoutManager = layoutManager


        injuriesViewModel = ViewModelProvider(this).get(InjuriesViewModel::class.java)

        injurysListAdapter = InjurysListAdapter(this, this, injuriesViewModel)
        injuryList.adapter = injurysListAdapter



        val callback : ItemTouchHelper.Callback = SimpleItemTouchHelperCallback(injurysListAdapter)
        mItemTouchHelper = ItemTouchHelper(callback)
        mItemTouchHelper.attachToRecyclerView(injuryList)



        injuriesViewModel.allInjuries.observe(this, Observer { injuries ->
            // Update the cached copy of the words in the adapter.
            injuries?.let { injurysListAdapter.injuries = ArrayList(it) }
        })

        val fab : View = findViewById(R.id.floatingActionButton)
        fab.setOnClickListener {
            /*val intent = Intent(context, InjuryDetailActivity::class.java)
            intent.putExtra(EXTRA_INJURY, "New injury")
            context.startActivity(intent)*/
            val currentDateTime = LocalDateTime.now()
            val exerciseNames = getString(R.string.exercise)
            val injury = Injury("New Injury", currentDateTime.format(DateTimeFormatter.ISO_DATE), "", Rehab(exerciseNames, exerciseNames, exerciseNames, exerciseNames, exerciseNames, 0))
            injuriesViewModel.insert(injury)
        }

    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_settings -> {
            val intent = Intent(this, SettingsActivity::class.java)
            this.startActivity(intent)
            true
        }
        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.mainmenu, menu)
        return true
    }

    override fun onStartDrag(viewHolder: RecyclerView.ViewHolder) {
        mItemTouchHelper.startDrag(viewHolder)
    }
}
