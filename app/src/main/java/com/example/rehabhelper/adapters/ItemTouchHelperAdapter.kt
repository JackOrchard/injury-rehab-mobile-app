package com.example.rehabhelper.adapters

import androidx.recyclerview.widget.RecyclerView

interface ItemTouchHelperAdapter {

    fun onItemDismiss(position: Int) : Unit

    fun onItemMove(fromPosition: Int, toPosition: Int): Boolean
}

interface ItemTouchHelperViewHolder {
    fun onItemClear() : Unit

    fun onItemSelected() : Unit
}

interface OnStartDragListener {
    fun onStartDrag(viewHolder: RecyclerView.ViewHolder) : Unit
}
