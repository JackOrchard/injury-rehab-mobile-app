package com.example.rehabhelper.adapters


/*class LoadDatabaseTask(adapter: InjurysListAdapter) : AsyncTask<Unit, Unit, InjuryDatabase?>() {
    private val adapter = WeakReference(adapter)

    override fun doInBackground(vararg p0: Unit?): InjuryDatabase? {
        var database: InjuryDatabase? = null
        adapter.get()?.let {
            database = Room.databaseBuilder(it.context!!.applicationContext, InjuryDatabase::class.java, "injuries").build()
        }
        return database
    }

    override fun onPostExecute(database: InjuryDatabase?) {
        adapter.get()?.let {
            it.database = database
        }
    }

}



class LoadInjuriesTask(private val database: InjuryDatabase,
                    private val adapter: InjurysListAdapter) : AsyncTask<Unit, Unit, List<Injury>>() {
    override fun doInBackground(vararg p0: Unit?): List<Injury> {
        val songDao = database.injuryDao()
        return songDao.getAll()
    }

    override fun onPostExecute(injuries: List<Injury>) {
        adapter.injuries = injuries.toMutableList()
    }
}


class NewInjuryTask(private val database: InjuryDatabase,
                  private val injury: Injury) : AsyncTask<Unit, Unit, Unit>() {
    override fun doInBackground(vararg p0: Unit?) {
        injury.id = database.injuryDao().insert(injury)
    }
}

class DeleteInjuryTask(private val database: InjuryDatabase,
                     private val injury: Injury) : AsyncTask<Unit, Unit, Unit>() {
    override fun doInBackground(vararg p0: Unit?) {
        database.injuryDao().delete(injury)
    }
}

class UpdateSongTask(private val database: InjuryDatabase,
                     private val injury: Injury) : AsyncTask<Unit, Unit, Unit>() {
    override fun doInBackground(vararg p0: Unit?) {
        database.injuryDao().update(injury)
    }
}

class GetInjuryTask(private val database: InjuryDatabase, val injuryId : String, val callback : (injury: Injury) -> Unit): AsyncTask<Unit, Unit, LiveData<Injury>>() {
    override fun doInBackground(vararg p0: Unit?) : LiveData<Injury> {
        val injuryDao = database.injuryDao()
        val injury = injuryDao.getInjury(injuryId)
        println("doInBackground get injury ")
        return injury
    }

    override fun onPostExecute(result: LiveData<Injury>) {
        println("onPostExecute get injury")
    }
}*/





