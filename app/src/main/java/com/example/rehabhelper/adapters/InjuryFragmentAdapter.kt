
package com.example.rehabhelper.adapters
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.rehabhelper.models.Injury
import com.example.rehabhelper.viewmodels.InjuryViewModel
import com.example.rehabhelper.views.InjuryFragment
import com.example.rehabhelper.views.RehabFragment


class PageCollectionAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle, val injuryId: String) : FragmentStateAdapter(fragmentManager, lifecycle) {

    private val itemsCount = 2

    override fun getItemCount(): Int = itemsCount




    override fun createFragment(position: Int): Fragment {
        var fragment: Fragment? = null
        when (position) {
            0 -> fragment =
                injuryFragmentInstance()
            1 -> fragment = rehabFragmentInstance()
        }
        return fragment!!
    }

    fun injuryFragmentInstance() : InjuryFragment {
        var bundle: Bundle = Bundle()
        bundle.putString(EXTRA_INJURY, injuryId)
        var injuryFragment = InjuryFragment()
        injuryFragment.arguments = bundle
        return injuryFragment
    }

    fun rehabFragmentInstance() : RehabFragment {
        var bundle: Bundle = Bundle()
        bundle.putString(EXTRA_INJURY, injuryId)
        var rehabFragment = RehabFragment()
        rehabFragment.arguments = bundle
        return rehabFragment
    }



}


