package com.example.rehabhelper.adapters

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewGroup
import androidx.core.view.MotionEventCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.example.rehabhelper.R
import com.example.rehabhelper.models.Injury
import com.example.rehabhelper.viewmodels.InjuriesViewModel
import com.example.rehabhelper.viewmodels.InjuryViewModel
import com.example.rehabhelper.views.InjuryDetailActivity
import kotlinx.android.synthetic.main.injury_row.view.*
import java.util.*
import kotlin.collections.ArrayList


const val EXTRA_INJURY = "com.example.EventTracker.INJURY"

class InjurysListAdapter(val context: Activity?, dragListener: OnStartDragListener, val injuriesViewModel: InjuriesViewModel) :
    RecyclerView.Adapter<InjurysListAdapter.MyViewHolder>(), ItemTouchHelperAdapter {

    private val mDragStartListener : OnStartDragListener = dragListener


    var injuries: ArrayList<Injury> = arrayListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view), ItemTouchHelperViewHolder
    {
        val textView = view.injuryTitleText
        val imageView = view.imageView
        val dateView = view.injuryDateText
        val card = view.injuryCard

        override fun onItemClear() {
            card.setBackgroundColor(0)
        }

        override fun onItemSelected() {
            card.setBackgroundColor(Color.RED)
        }

    }


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MyViewHolder {
        // create a new view
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.injury_row, parent, false) as View
        // set the view's size, margins, paddings and layout parameters
        return MyViewHolder(
            view
        )
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        val injury = injuries[position]
        holder.textView.text = injury.name
        holder.dateView.text= injury.date

        holder.view.setOnClickListener {
            val intent = Intent(context, InjuryDetailActivity::class.java)
            intent.putExtra(EXTRA_INJURY, injury.id.toString())
            context!!.startActivity(intent)
        }

        holder.imageView.setOnTouchListener(OnTouchListener { v, event ->
            if (event.actionMasked == MotionEvent.ACTION_DOWN) {
                mDragStartListener.onStartDrag(holder)
            }
            false
        })


    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = injuries.size


    override fun onItemDismiss(position: Int) {
        val injury = injuries[position]
        injuries.removeAt(position)
        notifyItemRemoved(position)
        injuriesViewModel.delete(injury)

    }

    override fun onItemMove(fromPosition: Int, toPosition: Int): Boolean {
        Collections.swap(injuries, fromPosition, toPosition)
        notifyItemMoved(fromPosition, toPosition)
        return true
    }

}