package com.example.rehabhelper.adapters

import android.app.Notification
import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.example.rehabhelper.R
import com.google.android.gms.location.ActivityTransitionResult
import com.google.android.gms.location.DetectedActivity


class ActivityRecognitionReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {

        if (ActivityTransitionResult.hasResult(intent)) {
            val result = ActivityTransitionResult.extractResult(intent)!!
            for (event in result.transitionEvents) {
                val notification = Notification.Builder(context, Notification.CATEGORY_REMINDER).run {
                    setSmallIcon(R.drawable.rehab_notification)
                    setContentTitle("Injury Alert!")
                    setContentText("You shouldn't be " + toActivityString(event.activityType))
                    setAutoCancel(true)
                    build()
                }

                val manager = context?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                manager.notify(0, notification)


            }
        }


    }

    fun toActivityString(activity : Int) : String {

        var activityString = "exercise"
        when(activity) {
            DetectedActivity.WALKING -> activityString = "walking"
            DetectedActivity.RUNNING -> activityString = "running"
            DetectedActivity.ON_BICYCLE -> activityString = "biking"
            else -> activityString = "exercise"
        }
        return activityString
    }
}