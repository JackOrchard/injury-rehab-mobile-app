package com.example.rehabhelper

import android.Manifest
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Switch
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreference
import com.example.rehabhelper.adapters.ActivityRecognitionReceiver
import com.google.android.gms.location.ActivityRecognition
import com.google.android.gms.location.ActivityTransition
import com.google.android.gms.location.ActivityTransitionRequest
import com.google.android.gms.location.DetectedActivity

private const val TRANSITIONS_RECEIVER_ACTION = BuildConfig.APPLICATION_ID + "TRANSITIONS_RECEIVER_ACTION"

/**
 * A simple [Fragment] subclass.
 */
class SettingsFragment : PreferenceFragmentCompat() {

    val REQUEST_CODE_PERMISSIONS = 11
    private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.ACTIVITY_RECOGNITION)

    private val transitions = mutableListOf<ActivityTransition>()
    private lateinit var pendingIntent : PendingIntent

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)
        createNotificationChannel()
        val mTransitionsReceiver = ActivityRecognitionReceiver()
        LocalBroadcastManager.getInstance(context!!).registerReceiver(
            mTransitionsReceiver,
            IntentFilter(TRANSITIONS_RECEIVER_ACTION)
        )
        val intent = Intent(context!!, ActivityRecognitionReceiver::class.java)
        intent.action = TRANSITIONS_RECEIVER_ACTION
        pendingIntent = PendingIntent.getBroadcast(context!!, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        preferenceScreen.sharedPreferences.registerOnSharedPreferenceChangeListener { sharedPreferences, key ->
            val isWalking : Boolean = sharedPreferences!!.getBoolean("walking_switch", false)
            val isBiking : Boolean = sharedPreferences!!.getBoolean("biking_switch", false)
            val isRunning : Boolean = sharedPreferences!!.getBoolean("running_switch", false)
            checkPermissions()
            disableActivityTransitions()
            transitions.clear()
            if (isWalking) {
                transitions += ActivityTransition.Builder()
                    .setActivityType(DetectedActivity.WALKING)
                    .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                    .build()
            }
            if (isBiking) {
                transitions += ActivityTransition.Builder()
                    .setActivityType(DetectedActivity.ON_BICYCLE)
                    .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                    .build()
            }
            if (isRunning) {
                transitions += ActivityTransition.Builder()
                    .setActivityType(DetectedActivity.RUNNING)
                    .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                    .build()
            }
            enableActivityTransitions()
        }

    }

    fun sharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {

    }

    fun checkPermissions() {
        val runningQOrLater = android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q;
        if (runningQOrLater) {
            if (ContextCompat.checkSelfPermission(context!!,
                    Manifest.permission.ACTIVITY_RECOGNITION)
                != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activity!!,
                    arrayOf(Manifest.permission.ACTIVITY_RECOGNITION),
                    REQUEST_CODE_PERMISSIONS)
            }
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_CODE_PERMISSIONS -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    val text = "Permission to walk!"
                    val duration = Toast.LENGTH_SHORT

                    val toast = Toast.makeText(context!!, text, duration)
                    toast.show()
                } else {
                    //closeswitches
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }



    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "Injury Updates"
            val descriptionText = "Activity updates for injuries"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel =
                NotificationChannel(Notification.CATEGORY_REMINDER, name, importance).apply {
                    description = descriptionText
                }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                context!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    fun enableActivityTransitions() {
        println( "enableActivityTransitions()");

        val activityTransitionRequest = ActivityTransitionRequest(transitions)



        val task = ActivityRecognition.getClient(context)
            .requestActivityTransitionUpdates(activityTransitionRequest, pendingIntent)

        task.addOnSuccessListener {
            val text = "Notifications for Activity Transitions Enabled"
            val duration = Toast.LENGTH_SHORT

            val toast = Toast.makeText(context, text, duration)
            toast.show()
        }

    }

    fun disableActivityTransitions() {
        val task = ActivityRecognition.getClient(context)
            .removeActivityTransitionUpdates(pendingIntent)

        task.addOnSuccessListener {
            Log.e("MYCOMPONENT", "Unregistered for all transitions")
        }

        task.addOnFailureListener { e: Exception ->
            Log.e("MYCOMPONENT", e.message)
        }
    }

}
