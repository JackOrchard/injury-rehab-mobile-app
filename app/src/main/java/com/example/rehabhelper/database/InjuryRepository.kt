package com.example.rehabhelper.database

import androidx.lifecycle.LiveData
import com.example.rehabhelper.models.Injury

// Declares the DAO as a private property in the constructor. Pass in the DAO
// instead of the whole database, because you only need access to the DAO
class InjuryRepository(private val injuryDao: InjuryDao) {

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    val allInjuries: LiveData<List<Injury>> = injuryDao.getAll()


    fun getInjury(injuryId : String) = injuryDao.getInjury(injuryId)

    fun getStaticInjury(injuryId : String) = injuryDao.getStaticInjury(injuryId)

    suspend fun insert(injury: Injury) {
        injuryDao.insert(injury)
    }

    suspend fun update(injury: Injury) {
        injuryDao.update(injury)
    }

    suspend fun delete(injury: Injury) {
        injuryDao.delete(injury)
    }
}