package com.example.rehabhelper.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.rehabhelper.models.Injury

@Database(entities = [Injury::class], version = 1)
abstract class InjuryDatabase : RoomDatabase() {
    abstract fun injuryDao(): InjuryDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: InjuryDatabase? = null

        fun getDatabase(context: Context): InjuryDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    InjuryDatabase::class.java,
                    "injuries"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }

}
