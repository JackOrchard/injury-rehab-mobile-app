package com.example.rehabhelper.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.rehabhelper.models.Injury
import kotlinx.coroutines.flow.Flow

@Dao
interface InjuryDao {
    @Insert
    suspend fun insert(injury: Injury) : Long

    @Update
    suspend fun update(injury: Injury)

    @Delete
    suspend fun delete(injury: Injury)

    @Query("SELECT * FROM injuries")
    fun getAll(): LiveData<List<Injury>>

    @Query("SELECT * FROM injuries WHERE id=:injuryId LIMIT 1")
    fun getInjury(injuryId : String) : LiveData<Injury>

    @Query("SELECT * FROM injuries WHERE id=:injuryId LIMIT 1")
    fun getStaticInjury(injuryId : String) : Injury
}