package com.example.rehabhelper.models

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

data class Rehab(
    var exerciseOne : String,
    var exerciseTwo : String,
    var exerciseThree: String,
    var exerciseFour : String,
    var exerciseFive : String,
    var timesCompleted : Int = 0
)

@Entity(tableName = "injuries")
class Injury(var name: String, var date: String, var imageUrl: String, @Embedded val rehab : Rehab?) {
    @PrimaryKey(autoGenerate = true) var id: Long = 0
}